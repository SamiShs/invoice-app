﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace autoFactuur.Resources
{
    public partial class gegevens : UserControl
    {
        Form1 myform;
        Savingdata nieuwefactuur;
        public gegevens()
        {
            InitializeComponent();
        }

        public void setmyform(Form1 form)
        {
            myform = form;
        }
        public void setSavingdata(Savingdata data)
        {
            nieuwefactuur = data;
            try
            {
                textBox1.Text = nieuwefactuur.PersoonlijkeGegevens[0];
                textBox2.Text = nieuwefactuur.PersoonlijkeGegevens[1];
                textBox3.Text = nieuwefactuur.PersoonlijkeGegevens[2];
                textBox4.Text = nieuwefactuur.PersoonlijkeGegevens[3];
                textBox5.Text = nieuwefactuur.PersoonlijkeGegevens[4];
                textBox6.Text = nieuwefactuur.PersoonlijkeGegevens[5];
                textBox7.Text = nieuwefactuur.PersoonlijkeGegevens[6];
                textBox8.Text = "BE" + nieuwefactuur.PersoonlijkeGegevens[7];
                textBox9.Text = nieuwefactuur.FactuurNummer.ToString();
                if (nieuwefactuur.format == "Word")
                {
                    radioButton2.Checked = true;
                }
            }
            catch
            {
                MessageBox.Show("that one error");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            myform.currentwindow("startvenster");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!nieuwefactuur.unlocked)
            {
                void Creat_Form()
                {
                    TextBox textBox11 = new TextBox();
                    textBox11.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    textBox11.Location = new Point(12, 12);
                    textBox11.Name = "Ok";
                    textBox11.Size = new Size(242, 20);
                    textBox11.TabIndex = 2;
                    textBox11.Text = "Geef de ontgrendelingscode";
                    textBox11.Click += new EventHandler(textBox11_Click);
                    void textBox11_Click(object senderr, EventArgs ee)
                    {
                        textBox11.Text = "";
                    }
                    Form myf = new Form();
                    myf.FormBorderStyle = FormBorderStyle.FixedDialog;
                    myf.AutoScaleDimensions = new SizeF(6F, 13F);
                    myf.AutoScaleMode = AutoScaleMode.Font;
                    myf.ClientSize = new Size(270, 71);
                    Button button22 = new Button();
                    button22.Location = new Point(63, 38);
                    button22.Name = "button2";
                    button22.Size = new Size(132, 23);
                    button22.Text = "Invoeren";
                    button22.UseVisualStyleBackColor = true;
                    button22.Click += new EventHandler(button2_Click);
                    myf.Controls.Add(button22);
                    myf.Controls.Add(textBox11);
                    myf.ShowDialog();
                    void button2_Click(object sender_, EventArgs e_)
                    {
                        if (textBox11.Text == "Unlock-Full")
                        {
                            nieuwefactuur.unlocked = true;
                            MessageBox.Show("Code is goed opgeslagen, druk opnieuw op 'gegevens bewaren' aub.");
                            myf.Close();
                        }
                        else
                        {
                            MessageBox.Show("foutieve code!");
                            myf.Close();
                        }
                    
                    }
                }
                Creat_Form();

            }
            else
             {
                string temptext = textBox8.Text;
                if (textBox8.Text.Contains("BE"))
                {
                    temptext = textBox8.Text.Split('E')[1];
                }
                temptext = string.Join("", temptext.Split(' '));
                textBox8.Text = temptext.Substring(0, 2) + " " + temptext.Substring(2, 4) + " " + temptext.Substring(6, 4) + " " + temptext.Substring(10, 4);
                nieuwefactuur.PersoonlijkeGegevens = new string[] { textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text,
                    textBox6.Text,textBox7.Text, textBox8.Text};
                nieuwefactuur.format = radioButton1.Checked ? radioButton1.Text : radioButton2.Text;
                nieuwefactuur.FactuurNummer = int.Parse(textBox9.Text);
                System.Xml.Serialization.XmlSerializer serialthis = new System.Xml.Serialization.XmlSerializer(typeof(Savingdata));
                using (System.IO.TextWriter tw = new System.IO.StreamWriter("data.xml"))
                {
                    serialthis.Serialize(tw, nieuwefactuur);
                }
                myform.currentwindow("refresh");
                MessageBox.Show("gegevens bewaard!");
                myform.currentwindow("newfactuur");
            }                       
        }
    }
}

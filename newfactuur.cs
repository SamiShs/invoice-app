﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Xml.Serialization;
using mshtml;

namespace autoFactuur
{
    public partial class newfactuur : UserControl
    {
        Form1 myform;
        private Savingdata nieuwfactuur;
        float prijstotaal;
        double btw;
        float prijsuur = 48.91f;
        float prijswacht = 34.65f;
        float prijskm = 0.53971f;
        float prijsbtw = 1.21f;
        int wuur;
        int wmin;
        int puur;
        int pmin;
        string box5 = "";

        AutoCompleteStringCollection source;

        public newfactuur()
        {
            InitializeComponent(); 
        }

        public Savingdata passSavingdata()
        {
            return this.nieuwfactuur;
        }

        public void setform(Form1 form)
        {
            myform = form;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            myform.currentwindow("startvenster");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string[] voogden = new string[4];
            button1.Enabled = false;
            button1.Text = "bezig...";

            try
            {
                File.WriteAllBytes(@Path.GetTempPath() + "voorbeeld.docx", autoFactuur.Properties.Resources.voorbeeld);
            }
            catch
            {
                MessageBox.Show("open de applicatie als administrator aub");
                return;
            }
            Microsoft.Office.Interop.Word.Application ap;
            try
            {
                ap = new Microsoft.Office.Interop.Word.Application();
                ap.Quit();

            }
            catch
            {
                MessageBox.Show("kan microsoft word niet openen");
                return;
            }

            ap = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document voobeeldoc;
            try
            {
                voobeeldoc = ap.Documents.Open(
                @Path.GetTempPath()+ "voorbeeld.docx");
                voobeeldoc.Close();
            }
            catch
            {
                MessageBox.Show("geen machtiging voor Temmap");
                ap.Quit(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
                return;
            }
            voobeeldoc = ap.Documents.Open(
                @Path.GetTempPath() + "voorbeeld.docx");
            object missing = System.Reflection.Missing.Value;
            void searchNreplace(string find, string replace)
            {                
                Microsoft.Office.Interop.Word.Find fnd = voobeeldoc.Application.Selection.Find;
                fnd.ClearFormatting();
                fnd.Text = find;
                fnd.Replacement.ClearFormatting();
                fnd.Replacement.Text = replace;
                object replaceAll = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
                fnd.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref replaceAll, ref missing, ref missing, ref missing, ref missing);

            }

            string[] voorbeeldgegevens = new string[26]{"#naamvoornaam","#padres","#postcode","#kadres","#btw","#tel","#email",
                "datumgesprek", "dossiernummer", "naamvoogd", "taalkeuze", "wachttijduur", "wachttijdminut", "wachttijdeuro",
                "gesprekuur", "gesprekminut", "gesprekeuro", "zoveelmaal", "kilometer1", "kilometertotaal", "kilometereuro", "totaleuro",
                "btwkosteuro", "totalbtweuro", "datumhandtekening","fctrnummer" };
            string[] newgegevens = new string[26]{nieuwfactuur.PersoonlijkeGegevens[0],nieuwfactuur.PersoonlijkeGegevens[1],nieuwfactuur.PersoonlijkeGegevens[2],
                nieuwfactuur.PersoonlijkeGegevens[3],nieuwfactuur.PersoonlijkeGegevens[4],nieuwfactuur.PersoonlijkeGegevens[5],nieuwfactuur.PersoonlijkeGegevens[6],
                dateTimePicker1.Value.ToString("dd/MM/yyyy"), textBox2.Text, textBox1.Text, selecteerTaalToolStripMenuItem.Text,
                getcorrect(comboBox1.Text),getcorrect(comboBox2.Text),getcorrect(label10.Text),getcorrect(comboBox3.Text),getcorrect(comboBox4.Text),
                getcorrect(label12.Text),getcorrect("checkbox2") , getcorrect("textbox5") + " ", getcorrect("kilometertotaal"), getcorrect(label15.Text),
                getcorrect(label18.Text),btw.ToString("0.00")+ " €", getcorrect(label20.Text), DateTime.Today.ToString("dd/MM/yyyy"),DateTime.Today.ToString("yyyy") + nieuwfactuur.FactuurNummer};
            for (int i = 0; i < 26; i++)
            {
                searchNreplace(voorbeeldgegevens[i], newgegevens[i]);
            }
            char[] k = (string.Join("",nieuwfactuur.PersoonlijkeGegevens[7].Split(' '))).ToCharArray();
            string[] tempfile = new string[] { "#1", "#2", "#3", "#4", "#5", "#6", "#7", "#8", "#9", "#01", "#02", "#03", "#04", "#05" };
            for (int i=0; i < k.Length; i++)
            {
                searchNreplace(tempfile[i], k[i].ToString());
            }
            if (File.Exists(@Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\handtekening.png") || nieuwfactuur.FactuurNummer < 4000)
            {
                float[] posi = new float[2];
                foreach (Shape s in voobeeldoc.Shapes)
                {
                    if (s.Type == Microsoft.Office.Core.MsoShapeType.msoPicture)
                    {
                        posi[0] = s.Left;
                        posi[1] = s.Top;
                        s.Delete();
                        break;
                    }
                }
                if (File.Exists(@Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath + "\\handtekening.png")))
                {
                    Shape picshape = voobeeldoc.Shapes.AddPicture(@Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\handtekening.png");
                    picshape.Left = posi[0];
                    picshape.Top = posi[1];

                }
                

            }
            

        string getcorrect(string para)
            {
                if (para == "0 u" || para == "0 min")
                {
                    return para.TrimStart(new char[] { '0', ' ' }) ;
                }
                if (para == "0,00 €")
                {
                    return para.TrimStart(new char[] { '0',',', '0','0', ' ' });
                }
                if (para == "checkbox2")
                {
                    return checkBox2.Checked ? "2" : "1";
                }
                if (para == "kilometertotaal")
                {
                    return checkBox2.Checked ? (int.Parse(textBox5.Text) * 2).ToString()+" km" : textBox5.Text+" km";
                }
                if (para == "textbox5")
                {
                    if (box5.Contains("+"))
                    {
                        return box5;
                    }
                    else
                    {
                        return textBox5.Text;
                    }
                }
                return para;
            }
            string end = "";
            try
            {
                System.IO.Directory.CreateDirectory(@System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\facturen\\");
                if (nieuwfactuur.format == "Word")
                {
                    end = ".docx";
                    voobeeldoc.SaveAs2(@Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) +
                                "\\facturen\\" + nieuwfactuur.FactuurNummer + "-" + DateTime.Today.ToString("ddMMyyyy")+end);
                }
                else
                {
                    end = ".pdf";
                    voobeeldoc.SaveAs2(@Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) +
                                "\\facturen\\"+ nieuwfactuur.FactuurNummer + "-" + DateTime.Today.ToString("ddMMyyyy")+end, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF);
                }
                
            }
            catch
            {
                MessageBox.Show("creatie map en pdf document niet toegelaten!");
                voobeeldoc.Close(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
                ap.Quit(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
                File.Delete(@Path.GetTempPath() + "voorbeeld.doc");
                return;
            }
            
            
            voobeeldoc.Close(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
            ap.Quit(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
            File.Delete("voorbeeld.doc");
            System.Diagnostics.Process.Start(@System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\facturen\\"+ nieuwfactuur.FactuurNummer + "-" +
                DateTime.Today.ToString("ddMMyyyy")+ end);
            nieuwfactuur.FactuurNummerUpdate();
            XmlSerializer serialthis = new XmlSerializer(typeof(Savingdata));
            using (TextWriter tw = new StreamWriter("data.xml"))
            {
                serialthis.Serialize(tw, nieuwfactuur);
            }
            button1.Enabled = true;
            button1.Text = "MAAK FACTUUR";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            nieuwfactuur.VoogdToevoegen(textBox1.Text, textBox2.Text);
            nieuwfactuur.StadToevoegen(textBox3.Text, textBox4.Text);
            nieuwfactuur.KilometerToevoegen(textBox3.Text, textBox4.Text, textBox5.Text);
            XmlSerializer serialthis = new XmlSerializer(typeof(Savingdata));
            try
            {
                using (TextWriter tw = new StreamWriter("data.xml"))
                {
                    serialthis.Serialize(tw, nieuwfactuur);
                }
            }
            catch
            {
                MessageBox.Show("geen machtiging om data op te slaan");
                return;
            }
            MessageBox.Show("gegevens successvol opgeslagen");
        }

        private void pashtouToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selecteerTaalToolStripMenuItem.Text = "pashtou";
        }

        private void dariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selecteerTaalToolStripMenuItem.Text = "Dari";
        }

        private void textBox2_MouseClick(object sender, MouseEventArgs e)
        {
            source = new AutoCompleteStringCollection();
            source.AddRange(nieuwfactuur.DossierWeergave(textBox1.Text));
            textBox2.AutoCompleteCustomSource = source;
            textBox2.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(comboBox1.Text, out wuur))
            {
                wuur = int.Parse(comboBox1.Text.TrimEnd(new char[] { ' ', 'u' }));
            }
            else
            {
                wuur = int.Parse(comboBox1.Text);
            }

            if (!int.TryParse(comboBox2.Text, out wmin))
            {
                wmin = int.Parse(comboBox2.Text.TrimEnd(new char[] { ' ', 'm', 'i', 'n' }));
            }
            else
            {
                wmin = int.Parse(comboBox2.Text);
            }

            if (!int.TryParse(comboBox3.Text, out puur))
            {
                puur = int.Parse(comboBox3.Text.TrimEnd(new char[] { ' ', 'u' }));
            }
            else
            {
                puur = int.Parse(comboBox3.Text);
            }

            if (!int.TryParse(comboBox4.Text, out pmin))
            {
               pmin = int.Parse(comboBox4.Text.TrimEnd(new char[] { ' ', 'm', 'i', 'n' }));
            }
            else
            {
                pmin = int.Parse(comboBox4.Text);
            }
            label10.Text = ((wuur + wmin/60f) * prijswacht).ToString("0.00")+" €";
            label11.Text = ((wuur + wmin/60f) * prijswacht*prijsbtw).ToString("0.00") +" €";
            label12.Text = ((puur + pmin/60f) * prijsuur).ToString("0.00") +" €";
            label14.Text = ((puur + pmin/60f) * prijsuur * prijsbtw).ToString("0.00") + " €";
            int maal = checkBox2.Checked ? 2 : 1;
            double r = (Math.Round((int.Parse(textBox5.Text) * maal * prijskm) * 100)) / 100;
            //double r = Math.Floor((int.Parse(textBox5.Text) * maal * prijskm) * 100) / 100;
            label15.Text = (r).ToString() + " €";
            double rr = Math.Round((int.Parse(textBox5.Text) * maal * prijskm*prijsbtw) * 100) / 100;
            label16.Text = (rr).ToString("0.00") + " €";
            prijstotaal = ((wuur + wmin/60f) * prijswacht) + ((puur + pmin / 60f) * prijsuur);
            label18.Text = (Math.Round((prijstotaal+r)*100)/100).ToString("0.00") + " €";
            label20.Text = (Math.Round(((prijstotaal*prijsbtw)+rr)*100)/100).ToString("0.00") + " €";
            btw = (prijstotaal + r) * (prijsbtw - 1);
        }

        int buzz = 0;
        private void button5_Click(object sender, EventArgs e)
        {
            box5 = "";
            string tempkilometer = nieuwfactuur.KilometerWeergave(textBox3.Text, textBox4.Text);
            if (tempkilometer != "zero")
            {
                DialogResult dialogResult = MessageBox.Show("gevonden km: " + tempkilometer +
                "\n click 'yes' om te gebruik \n click 'no' om te updaten (viamichelin)", "Opgeslagen Afstanden", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    textBox5.Text = tempkilometer;
                }
                else if (dialogResult == DialogResult.No)
                {
                    Viamichelinzoeken();
                }
            }
            else
            {
                Viamichelinzoeken();
            }
                                    
            void Viamichelinzoeken()
            {                
                WebBrowser webBrowser1 = new WebBrowser();
                System.Threading.Thread newmetthread = new System.Threading.Thread(new System.Threading.ThreadStart(newmethod));
                string viamichelenurl = "https://nl.viamichelin.be/web/Routes/Route-" + textBox3.Text + "-naar-" + textBox4.Text;
                newmetthread.Start();
                //newmetthread.
                webBrowser1.Navigate(viamichelenurl);
                webBrowser1.Visible = false;
                webBrowser1.ScriptErrorsSuppressed = true;
                
                void newmethod()
                {
                    if (buzz == 0)
                    {
                        buzz = 1;
                        string k = "0";
                        while (true)
                        {
                            string htmltext = " ";
                            System.Threading.Thread.Sleep(2000);
                            IHTMLDocument2 mycdoc;
                            webBrowser1.Invoke((MethodInvoker)delegate()
                            {mycdoc = (IHTMLDocument2)webBrowser1.Document.DomDocument;
                             htmltext = mycdoc.activeElement.outerHTML;
                            });
                            System.Threading.Thread.Sleep(100);
                            try
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    int indexstart = htmltext.IndexOf("summary-current-details") + 25;
                                    int indexend = htmltext.IndexOf("itinerary-show-roadsheet-container") + 35;
                                    htmltext = htmltext.Substring(indexstart, indexend - indexstart);
                                    if (!k.Contains("km"))
                                    {
                                        indexstart = htmltext.IndexOf("\"summary-details-em\">") + 21;
                                        htmltext = htmltext.Substring(indexstart, htmltext.Length - indexstart);
                                        indexend = htmltext.IndexOf("</em>");
                                        k = htmltext.Substring(0, indexend);                                        
                                    }
                                    else
                                    {
                                        webBrowser1.Invoke((MethodInvoker)delegate()
                                        {
                                            webBrowser1.Navigate("about:blank");
                                            System.Windows.Forms.Application.DoEvents();
                                            webBrowser1.Url = new Uri("about:blank");
                                            webBrowser1.Dispose();
                                        });
                                        break;
                                    }
                                    buzz += 1;
                                }
                                if (k.Contains("km"))
                                {
                                    break;
                                }

                                if (buzz == 5)
                                {
                                    webBrowser1.Invoke((MethodInvoker)delegate ()
                                    {
                                        webBrowser1.Url = new Uri("about:blank");
                                        webBrowser1.Dispose();
                                    });
                                    buzz = 0;
                                    MessageBox.Show("html  bestand bevat geen km");
                                    break;
                                }
                                
                            }
                            catch
                            {
                                if (buzz == 5)
                                {
                                    webBrowser1.Invoke((MethodInvoker)delegate ()
                                    {
                                        webBrowser1.Url = new Uri("about:blank");
                                        webBrowser1.Dispose();
                                    });
                                    MessageBox.Show("kan niet laden, probeer opnieuw");
                                    buzz = 0;
                                    return;
                                }
                                else
                                {
                                    buzz += 1;
                                    System.Threading.Thread.Sleep(100);
                                }
                            }
                        }
                        k = k.Split(' ')[0];
                        k = k.Replace(".5", ".6");
                        double z = Math.Round(double.Parse(k, System.Globalization.CultureInfo.InvariantCulture));
                        if (z > 100)
                        {
                            DialogResult dialogResult1 = MessageBox.Show("gevonden km: " + z.ToString() +
                                "\n click 'yes' om  te wijzigen naar 100 km \n click 'no' om niet te wijzigen","afronden naar 100", MessageBoxButtons.YesNo);
                            if (dialogResult1 == DialogResult.Yes)
                            {
                                textBox5.Invoke((MethodInvoker)delegate { textBox5.Text = "100"; });
                                return;
                            }
                            else if (dialogResult1 == DialogResult.No)
                            {
                            }

                        }
                        DialogResult dialogResult = MessageBox.Show("gevonden km: " + k, "Viamichelin kilometers");
                        textBox5.Invoke((MethodInvoker)delegate { textBox5.Text = Math.Round(
                            double.Parse(k, System.Globalization.CultureInfo.InvariantCulture)).ToString(); });
                        buzz = 0;
                    }
                }
            }
        }

        private void newfactuur_Load(object sender, EventArgs e)
        {
            try
            {
                XmlSerializer deserialthis = new XmlSerializer(typeof(Savingdata));
                TextReader tr = new StreamReader(@System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\data.xml");
                nieuwfactuur = (Savingdata)deserialthis.Deserialize(tr);
                tr.Close();
            }
            catch
            {
                nieuwfactuur = new Savingdata();
                nieuwfactuur.Steden = new string[] {"Aalst", "Aarlen", "Aarschot", "Aat", "Andenne", "Antoing", "Antwerpen", "Bastenaken", "Beaumont", "Beauraing", "Bergen",
                 "Beringen", "Bilzen", "Binche", "Blankenberge", "Borgloon", "Borgworm", "Bouillon", "Bree", "Brugge", "Brussel", "Charleroi",
                 "Châtelet", "Chièvres", "Chimay", "Chiny", "Ciney", "Couvin", "Damme", "Deinze", "Dendermonde","Diest", "Diksmuide",
                 "Dilsen-Stokkem", "Dinant", "Doornik", "Durbuy", "Edingen", "Eeklo", "Eupen", "Fleurus", "Florenville", "Fontaine-l'Evêque",
                 "Fosses-la-Ville", "Geel", "Geldenaken", "Gembloers", "Genepiën", "Genk", "Gent", "Geraardsbergen", "Gistel", "'s-Gravenbrakel",
                 "Halen", "Halle", "Hamont-Achel", "Hannuit", "Harelbeke", "Hasselt", "Herentals", "Herk-de-Stad", "Herstal", "Herve", "Hoei",
                 "Hoogstraten", "Houffalize", "Ieper", "Izegem", "Komen-Waasten", "Kortrijk", "La Louvière", "La Roche-en-Ardenne", "Landen",
                 "Le Rœulx", "Lessen", "Leuze-en-Hainaut", "Leuven", "Lier", "Limburg", "Lo-Reninge", "Lokeren", "Lommel", "Luik", "Maaseik",
                 "Malmedy", "Marche-en-Famenne", "Mechelen", "Menen", "Mesen", "Moeskroen", "Mortsel", "Namen", "Neufchâteau", "Nieuwpoort",
                "Nijvel", "Ninove", "Oostende", "Ottignies-Louvain-la-Neuve", "Oudenaarde", "Oudenburg", "Peer", "Péruwelz", "Philippeville",
                 "Poperinge", "Rochefort", "Roeselare", "Ronse", "Saint-Ghislain", "Saint-Hubert", "Sankt Vith", "Scherpenheuvel-Zichem",
                 "Seraing", "Sint-Niklaas", "Sint-Truiden", "Spa", "Stavelot", "Thuin", "Tielt", "Tienen", "Tongeren", "Torhout", "Turnhout", "Verviers",
                "Veurne", "Vilvoorde", "Virton", "Walcourt", "Waregem", "Waver", "Wervik", "Wezet", "Zinnik", "Zottegem", "Zoutleeuw"};
                nieuwfactuur.PersoonlijkeGegevens = new string[] { " Sharif Samirullah", "Westergemstraat 20", "9032 Wondelgem", "Poperingestraat 107  9000 Gent"
                ,"BE0886.965.426","0485 61 09 43","s.sharif93@outlouk.com","29 3630 8381 0464"};
                nieuwfactuur.FactuurNummer = 4000;
                //"#1","#2","#3","#4","#5","#6","#7","#8","#9","#01","#02","#03","#04","#05"};
                nieuwfactuur.Voogden = new string[] { "Sas Leo", "Slootmans Franky" };
                nieuwfactuur.Dossiers = new string[] { "33439;", "Sultani Ebadullah;" };
                string zero = new string('+', nieuwfactuur.Steden.Length).Replace("+", "zero;");
                nieuwfactuur.StedenKilometer = new string[nieuwfactuur.Steden.Length];
                for (int i = 0; i < nieuwfactuur.Steden.Length; i++)
                {
                    nieuwfactuur.StedenKilometer[i] = zero;
                }

            }
            source = new AutoCompleteStringCollection();
            source.AddRange(nieuwfactuur.Voogden);
            textBox1.AutoCompleteCustomSource = source;
            source = new AutoCompleteStringCollection();
            source.AddRange(nieuwfactuur.Steden);
            textBox3.AutoCompleteCustomSource = source;
            textBox4.AutoCompleteCustomSource = source;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form2 nieuwform = new Form2(nieuwfactuur);
            if (nieuwform.ShowDialog() == DialogResult.OK)
            {
                textBox5.Text = Math.Round(nieuwform.kilometers).ToString();
                textBox3.Text = "via";
                textBox4.Text = "via";
                box5 = nieuwform.box5;
            }
        }
    }
}

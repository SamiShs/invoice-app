﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;

namespace autoFactuur
{
    public partial class Form2 : Form
    {
        public string box5 = "";
        public double kilometers;
        Label[] labelx = new Label[10];
        TextBox[] textboxx = new TextBox[10];
        int x = 0;
        Savingdata nieuwfactuur;
        AutoCompleteStringCollection source;
        public Form2(Savingdata save)
        {
            InitializeComponent();
            nieuwfactuur = save;

        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            Label[] labelx = new Label[10];
            TextBox[] textboxx = new TextBox[10];
            labelx[x] = new Label();
            textboxx[x] = new TextBox();
            tableLayoutPanel1.Controls.Remove(this.textBox3);
            tableLayoutPanel1.Controls.Remove(label3);
            tableLayoutPanel1.ColumnCount += 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.67327F));
            tableLayoutPanel1.Controls.Add(textBox3, 2+x+1, 1);
            tableLayoutPanel1.Controls.Add(label3, 2+x+1, 0);


            labelx[x].AutoSize = true;
            labelx[x].Dock = System.Windows.Forms.DockStyle.Fill;
            labelx[x].Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelx[x].Location = new System.Drawing.Point(168, 0);
            labelx[x].Name = "label2";
            labelx[x].Size = new System.Drawing.Size(159, 27);
            labelx[x].TabIndex = 2;
            labelx[x].Text = "via";
            labelx[x].TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            textboxx[x].Dock = System.Windows.Forms.DockStyle.Fill;
            textboxx[x].Location = new System.Drawing.Point(168, 30);
            textboxx[x].Name = "textBox2";
            textboxx[x].Size = new System.Drawing.Size(159, 20);
            textboxx[x].TabIndex = 5;
            textboxx[x].AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            textboxx[x].AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            textboxx[x].AutoCompleteCustomSource = source;

            tableLayoutPanel1.Controls.Add(labelx[x], 2 + x, 0);
            tableLayoutPanel1.Controls.Add(textboxx[x], 2 + x,1);
            x += 1;
            if (x >= 2)
            {
                this.Size = new Size(this.Size.Width + 70, this.Size.Height);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Text = "bezig...";
            button2.Enabled = false;
            int i = 0;
            kilometers = 0;
            WebBrowser webBrowser1 = new WebBrowser();
            firstsearch();
            
            void firstsearch()
            {
                string stad1 = tableLayoutPanel1.GetControlFromPosition(i, 1).Text;
                string stad2 = tableLayoutPanel1.GetControlFromPosition(i + 1, 1).Text;

                string tempkilometer = nieuwfactuur.KilometerWeergave(stad1,stad2);
                if (tempkilometer != "zero")
                {
                    DialogResult dialogResult = MessageBox.Show("voor " + stad1+"-"+ stad2 + "gevonden km: " + tempkilometer +
                    "\n click 'yes' om te gebruik \n click 'no' om te updaten (viamichelin)", "Opgeslagen Afstanden", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        kilometers += int.Parse(tempkilometer);
                        i += 1;
                        if(i != x + 2)
                        {
                            firstsearch();
                        }
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        Viamichelinzoeken(stad1, stad2);
                    }
                }
                else
                {
                    Viamichelinzoeken(stad1, stad2);
                }
            }
            
            void Viamichelinzoeken(string start, string finish)
            {
                int buzz = 0;

                System.Threading.Thread threads = new System.Threading.Thread(new System.Threading.ThreadStart(newmethod));
                string viamichelenurl = "https://nl.viamichelin.be/web/Routes/Route-" + start + "-naar-" + finish;
                threads.Start();
                if (i == 0)
                {
                    webBrowser1.Navigate(viamichelenurl);
                    webBrowser1.Visible = false;
                    webBrowser1.ScriptErrorsSuppressed = true;
                }
                else
                {
                    webBrowser1.Invoke((MethodInvoker)delegate ()
                    {
                        webBrowser1.Navigate(viamichelenurl);
                        webBrowser1.Visible = false;
                        webBrowser1.ScriptErrorsSuppressed = true;
                    });
                }

                void newmethod()
                {
                    if (buzz == 0)
                    {
                        buzz = 1;
                        string k = "0";
                        while (true)
                        {
                            string htmltext = " ";
                            System.Threading.Thread.Sleep(2000);
                            IHTMLDocument2 mycdoc;
                            webBrowser1.Invoke((MethodInvoker)delegate ()
                            {
                                mycdoc = (IHTMLDocument2)webBrowser1.Document.DomDocument;
                                htmltext = mycdoc.activeElement.outerHTML;
                            });
                            System.Threading.Thread.Sleep(100);
                            try
                            {
                                for (int j = 0; j < 4; j++)
                                {
                                    int indexstart = htmltext.IndexOf("summary-current-details") + 25;
                                    int indexend = htmltext.IndexOf("itinerary-show-roadsheet-container") + 35;
                                    htmltext = htmltext.Substring(indexstart, indexend - indexstart);
                                    if (!k.Contains("km"))
                                    {
                                        indexstart = htmltext.IndexOf("\"summary-details-em\">") + 21;
                                        htmltext = htmltext.Substring(indexstart, htmltext.Length - indexstart);
                                        indexend = htmltext.IndexOf("</em>");
                                        k = htmltext.Substring(0, indexend);
                                    }
                                    else
                                    {
                                        webBrowser1.Invoke((MethodInvoker)delegate ()
                                        {
                                            webBrowser1.Navigate("about:blank");
                                            Application.DoEvents();
                                            webBrowser1.Url = new Uri("about:blank");
                                        });
                                        break;
                                    }
                                    buzz += 1;
                                }
                                if (k.Contains("km"))
                                {
                                    break;
                                }

                                if (buzz == 5)
                                {
                                    webBrowser1.Invoke((MethodInvoker)delegate ()
                                    {
                                        webBrowser1.Url = new Uri("about:blank");
                                    });
                                    buzz = 0;
                                    MessageBox.Show("html  bestand bevat geen km");
                                    break;
                                }

                            }
                            catch
                            {
                                if (buzz == 5)
                                {
                                    webBrowser1.Invoke((MethodInvoker)delegate ()
                                    {
                                        webBrowser1.Url = new Uri("about:blank");
                                    });
                                    MessageBox.Show("kan niet laden, probeer opnieuw");
                                    buzz = 0;
                                    return;
                                }
                                else
                                {
                                    buzz += 1;
                                    System.Threading.Thread.Sleep(100);
                                }
                            }
                        }
                        k = k.Split(' ')[0];
                        k = k.Replace(".5", ".6");
                        double z = Math.Round(double.Parse(k, System.Globalization.CultureInfo.InvariantCulture));
                        if (z > 100)
                        {
                            DialogResult dialogResult1 = MessageBox.Show("voor " + start + "-" + finish + "gevonden km: " + z.ToString() +
                                "\n click 'yes' om  te wijzigen naar 100 km \n click 'no' om niet te wijzigen", "afronden naar 100", MessageBoxButtons.YesNo);
                            if (dialogResult1 == DialogResult.Yes)
                            {
                                kilometers += 100;
                                return;
                            }
                            else if (dialogResult1 == DialogResult.No)
                            {
                            }

                        }
                        DialogResult dialogResult = MessageBox.Show("voor " + start + "-" + finish + " gevonden km: " + k, "Viamichelin kilometers");
                        kilometers += double.Parse(k, System.Globalization.CultureInfo.InvariantCulture);
                        label4.Invoke((MethodInvoker)delegate ()
                        {
                            label4.Text += k.ToString() + " + ";

                        });
                        box5 += k.ToString();
                        

                        if (i != x + 1)
                        {
                            i += 1;
                            box5 += "+";
                            firstsearch();
                        }
                        else
                        {
                            buzz = 0;
                            System.Threading.Thread.Sleep(1000);
                            MessageBox.Show("total kms =" + Math.Round(kilometers).ToString());
                            DialogResult = DialogResult.OK;
                        }
                        
                    }
                }
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            source = new AutoCompleteStringCollection();
            source.AddRange(nieuwfactuur.Steden);
            textBox1.AutoCompleteCustomSource = source;
            textBox3.AutoCompleteCustomSource = source;
            textBox2.AutoCompleteCustomSource = source;
            textBox1.Text = "Gent";
            textBox2.Text = "Leuven";
            textBox3.Text = "Antwerpen";

        }
    }
}

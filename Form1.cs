﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace autoFactuur
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void currentwindow(string str)
        {
            if (str == "newfactuur")
            {
                newfactuur2.BringToFront();
            }
            else if (str == "startvenster")
            {
                startvenster1.BringToFront();
            }
            else if (str == "gegevens")
            {
                gegevens1.BringToFront();
            }
            else if (str == "refresh")
            {
                newfactuur2.Refresh();
            }
        }
        Point lastPoint;
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                 {
                    this.Left += e.X - lastPoint.X;
                    this.Top += e.Y - lastPoint.Y;
                 }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            startvenster1.BringToFront();
            startvenster1.setmyform(this);
            newfactuur2.setform(this);
            gegevens1.setmyform(this);
            gegevens1.setSavingdata(newfactuur2.passSavingdata());
        }

        private void startvenster1_Load(object sender, EventArgs e)
        {

        }
    }
}

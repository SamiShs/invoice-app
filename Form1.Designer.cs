﻿namespace autoFactuur
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.gegevens1 = new autoFactuur.Resources.gegevens();
            this.newfactuur2 = new autoFactuur.newfactuur();
            this.startvenster1 = new autoFactuur.startvenster();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(102, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(360, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "automatische factuur maker";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            // 
            // gegevens1
            // 
            this.gegevens1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.gegevens1.Location = new System.Drawing.Point(0, 48);
            this.gegevens1.Name = "gegevens1";
            this.gegevens1.Size = new System.Drawing.Size(550, 450);
            this.gegevens1.TabIndex = 4;
            // 
            // newfactuur2
            // 
            this.newfactuur2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.newfactuur2.Location = new System.Drawing.Point(0, 49);
            this.newfactuur2.Name = "newfactuur2";
            this.newfactuur2.Size = new System.Drawing.Size(550, 450);
            this.newfactuur2.TabIndex = 3;
            // 
            // startvenster1
            // 
            this.startvenster1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.startvenster1.Location = new System.Drawing.Point(1, 48);
            this.startvenster1.Name = "startvenster1";
            this.startvenster1.Size = new System.Drawing.Size(550, 450);
            this.startvenster1.TabIndex = 1;
            this.startvenster1.Load += new System.EventHandler(this.startvenster1_Load);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::autoFactuur.Properties.Resources.close;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(503, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 45);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(550, 500);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.startvenster1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gegevens1);
            this.Controls.Add(this.newfactuur2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private startvenster startvenster1;
        private System.Windows.Forms.Button button1;
        private newfactuur newfactuur2;
        private Resources.gegevens gegevens1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace autoFactuur
{
    [Serializable()]
    public class Savingdata : ISerializable
    {
        public string[] Voogden { get; set; }
        public string[] Dossiers { get; set; }
        public string[] Steden { get; set; }
        public string[] StedenKilometer { get; set; }
        public int FactuurNummer = 4001;
        public string[] PersoonlijkeGegevens { get; set; }
        public bool unlocked = true;
        public string format = "PDF";
        public Savingdata() { }

        public void VoogdToevoegen(string nieuwvoogd, string nieuwdossier)
        {
            if (!Voogden.Contains(nieuwvoogd))
            {
                List<string> itemsList = Voogden.ToList<string>();
                itemsList.Add(nieuwvoogd);
                Voogden = itemsList.ToArray();
                itemsList = Dossiers.ToList<string>();
                itemsList.Add(nieuwdossier);
                Dossiers = itemsList.ToArray();
            }
            int index = Array.IndexOf(Voogden, nieuwvoogd);
            string[] bezig = Dossiers[index].Split(';');
            if (!(bezig.Contains(nieuwdossier)))
            {
                Dossiers[index] = Dossiers[index] + nieuwdossier + ";";
            }            
        }

        public string[] DossierWeergave(string voogd)
        {
            int index = Array.IndexOf(Voogden, voogd);
            if (index == -1)
            {
                return new string[]{ };
            }

            return (string[])Dossiers[index].Split(';');
        }

        public void StadToevoegen(string stadver, string stadaan)
        {
            if (!Steden.Contains(stadver) || !Steden.Contains(stadaan))
            {
                int j = 0;
                if (!Steden.Contains(stadaan)){
                    j += 1;
                }
                if (!Steden.Contains(stadver))
                {
                    j += 1;
                }
                string[] tempsteden = new string[Steden.Length + j];
                string[] tempkilometer = new string[Steden.Length + j];

                for (int i = 0; i < Steden.Length; i++)
                {
                    tempsteden[i] = Steden[i];
                    tempkilometer[i] = StedenKilometer[i] + "zero;";
                }
                string zero = new string('+', Steden.Length).Replace("+", "zero;");
                if (!Steden.Contains(stadver)){
                    tempkilometer[Steden.Length + j-1] = zero;
                    tempsteden[Steden.Length + j-1] = stadver;
                    j -= 1;
                }
                if (!Steden.Contains(stadaan))
                {
                    tempsteden[Steden.Length + j-1] = stadaan;
                    tempkilometer[Steden.Length + j-1] = zero;
                }
                Steden = tempsteden;
                StedenKilometer = tempkilometer;
            }
        }

        public void KilometerToevoegen(string stadver,string stadaan,string kilometer)
        {
            int indexone = Array.IndexOf(Steden, stadver);
            int indextwo = Array.IndexOf(Steden, stadaan);
            string[] tempkilometer = StedenKilometer[indexone].Split(';');
            tempkilometer[indextwo] = kilometer;
            StedenKilometer[indexone] = string.Join(";", tempkilometer);
        }

        public string KilometerWeergave(string stadver, string stadaan)
        {
            int indexone = Array.IndexOf(Steden, stadver);
            int indextwo = Array.IndexOf(Steden, stadaan);
            if (indexone == -1 || indextwo == -1)
            {
                return "zero";
            }

            string[] tempkilometer = StedenKilometer[indexone].Split(';');
            return tempkilometer[indextwo];
        }

        public void FactuurNummerUpdate()
        {
            FactuurNummer = FactuurNummer+1;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("PersoonlijkeGegevens", PersoonlijkeGegevens);
            info.AddValue("Voogden", Voogden);
            info.AddValue("Dossiers", Dossiers);
            info.AddValue("Steden", Steden);
            info.AddValue("StedenKilometer", StedenKilometer);
            info.AddValue("Factuurnummer", FactuurNummer);
            info.AddValue("unlocked", unlocked);
            info.AddValue("format", format);
            
        }

        public Savingdata(SerializationInfo info, StreamingContext context)
        {
            PersoonlijkeGegevens = (string[])(info.GetValue("PersoonlijkeGegevens", typeof(string[])));
            Voogden = (string[])info.GetValue("Voogden", typeof(string[]));
            Dossiers = (string[])info.GetValue("Dossiers", typeof(string[]));
            Steden = (string[])info.GetValue("Steden", typeof(string[]));
            StedenKilometer = (string[])info.GetValue("StedenKilometer", typeof(string[]));
            FactuurNummer = (int)(info.GetValue("Factuurnummer", typeof(int)));
            unlocked = (bool)(info.GetValue("unlocked", typeof(bool)));
            format = (string)(info.GetValue("format", typeof(string)));
        }
    }
}
